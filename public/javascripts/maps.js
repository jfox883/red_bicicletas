var mymap = L.map('mapid').setView([-34.6053004,-58.3838158], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiamZveDg4MyIsImEiOiJja2ViZnlkaDEwOGh1MnluaWs3cXRzd2JxIn0.P03pK_T5nGBnRly3DGKrTQ'
}).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: (result) => {
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.code + ' '+ bici.color}).addTo(mymap);
            console.log(bici);
        });
    }
})