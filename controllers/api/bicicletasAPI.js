var Bicicleta = require('../../models/bicicletas');

exports.bicicleta_list = (req, res) => {
    Bicicleta.find({}, (err, bicis) => {
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_crate = (req, res) => {
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici);

    res.status(200).json({
        Bicicletas: bici
    })
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeByCode(req.body.code, (err, delBici) =>{
        res.status(204).send();
    });
}

exports.bicicleta_update = (req, res) => {
    Bicicleta.findOneAndUpdate({ code: req.body.code_find }, {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubiciacio: [req.body.lat, req.body.lng]
    }, (err, doc) => {
        if (err) return res.send(500, {error: err});
        res.send('Succesfully updated.');
    });
}