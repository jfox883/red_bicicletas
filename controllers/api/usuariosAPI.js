var Usuario = require('../../models/usuarios');

exports.usuario_list = (req, res) => {
    Usuario.find({}, (err, doc) => {
        res.status(200).json({
            usuarios: doc
        });
    });
};

exports.usuario_create = (req, res) =>{
    var usuario = new Usuario({ usuario: req.body.usuario});
    usuario.save((err) => {
        res.status(200).json(usuario);
    });
};

exports.usuario_reserva = (req, res) => {
    Usuario.findById(req.body.id, (err, usuario) => {
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, (err) =>{
            if(err) console.log(err);
            res.status(200).send('Reservado!!!');
        });
    });
};