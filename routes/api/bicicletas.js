var express = require('express');
var router = express.Router();
var bicicletaControllerAPI = require('../../controllers/api/bicicletasAPI');
const bicicleta = require('../../models/bicicletas');

router.get('/', bicicletaControllerAPI.bicicleta_list);
router.post('/create', bicicletaControllerAPI.bicicleta_crate);
router.delete('/delete', bicicletaControllerAPI.bicicleta_delete);
router.post('/update', bicicletaControllerAPI.bicicleta_update);

module.exports = router;
