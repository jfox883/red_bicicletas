var express = require('express');
var router = express.Router();
var usuarioControllerAPI = require('../../controllers/api/usuariosAPI');
const usuario = require('../../models/usuarios');

router.get('/', usuarioControllerAPI.usuario_list);
router.post('/create', usuarioControllerAPI.usuario_create);
router.post('/reservar', usuarioControllerAPI.usuario_reserva);

module.exports = router;