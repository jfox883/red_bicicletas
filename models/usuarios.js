var mongoose = require('mongoose');
var Reserva = require('./reservas');
var Schema = mongoose.Schema;
const bcrpyt = require('bcrypt');
const saltRounds = 10;
const uniqueValidator = require('mongoose-unique-validator');

const validateEmail = (email) => {
    const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    return re.test(email);
}

var usuarioSchema = Schema({
    usuario: {
        type: String,
        trim: true,
        required: [true, 'El nombre de usuario es requerido']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es requerido'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor, corregir el email'],
        match: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
    },
    password: {
        type: String,
        required: [true, 'La contraseña es requerida']
    },
    passwordResetToken: String,
    passwordResetTokenExpire: Date,
    verificated: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya se encuentra registrado'});

usuarioSchema.pre('save', (next) => {
    if (this.isModified('password')){
        this.password = bcrpyt.hashSync(this.password, saltRounds);
    };
    next();
});

usuarioSchema.methods.validatePassword = function(password){
    return bcrpyt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    reserva.save(cb);
};

module.exports = mongoose.model('usuario', usuarioSchema);
