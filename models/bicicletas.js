var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletasSchema = Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2Dsphere', sparse: true}
    }
});

bicicletasSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletasSchema.methods.toString = () => 'code: ' + this.code + ' | color: ' + this.color;

bicicletasSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletasSchema.statics.add = function(a, cb){
    return this.create(a, cb);
};

bicicletasSchema.statics.findByCode = function(code, cb){
    return this.findOne({code: code}, cb);
};

bicicletasSchema.statics.removeByCode = function(code, cb){
    return this.deleteOne({code: code}, cb);
};


module.exports = mongoose.model('bicicleta', bicicletasSchema);