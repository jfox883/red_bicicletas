var mongoose = require('mongoose');
var bicicleta = require('../../models/bicicletas');

describe('Testing Bicicletas...', () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', () => console.log('We are connected to the database!'));  
        done();
    });

    afterEach((done) => {
        bicicleta.deleteMany({}, (err, success) => {
            if(err) console.log(err);
            done();
        });
    });

    describe('bicicleta.createInstance', () => {
        it('Crear una nueva instancia', (done) => {
            var a = bicicleta.createInstance(1, 'negro', 'urbana', [-34.65656, -58.23456]);
    
            expect(a.code).toBe(1);
            expect(a.color).toBe('negro');
            expect(a.modelo).toBe('urbana');
            expect(a.ubicacion[0]).toEqual(-34.65656);
            expect(a.ubicacion[1]).toEqual(-58.23456);
            done();
        });
    });

    describe('bicicleta.allBicis', () => {
        it('Comienza vacia...', (done) => {
            bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('bicicleta.add', () => {
        it('Agrega una bici', (done) => {
            var aBici = bicicleta.createInstance(1, 'verde', 'urbana', [-34.65656, -58.23456]);
            bicicleta.add(aBici, function(err, bicis){
                bicicleta.allBicis((err, bicis) => {
                    if(err) console.log(err);
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(1);
                    done();
                })
            });
        });
    });

    describe('bicicleta.findByCode', () => {
        it('Encontrar la bici...', (done) => {
            bicicleta.allBicis((err, bicis) => expect(bicis.length).toBe(0));

            var a = bicicleta.createInstance(1, 'azul', 'urbana', [-34.252525, -58.846589]);
            bicicleta.add(a, (err, newBicis) => {
                if(err) console.log(err);

                var b = bicicleta.createInstance(2, 'roja', 'montaña', [-34.252525, -58.846589]);
                bicicleta.add(b, (err, newBicis) => {
                    if(err) console.log(err);
                    
                    bicicleta.findByCode(1, (err, targetBici) => {
                        expect(targetBici.code).toBe(a.code);
                        expect(targetBici.color).toBe(a.color);
                        expect(targetBici.modelo).toBe(a.modelo);
                        done();
                    });
                    
                });
            });
        });
    });

    describe('bicicleta.removeByCode', () =>{
        it('Eliminar una bici', (done) =>{
            bicicleta.allBicis((err, bicis) => expect(bicis.length).toBe(0));

            var a = bicicleta.createInstance(1, 'azul', 'urbana', [-34.252525, -58.846589]);
            bicicleta.add(a, (err, newBicis) => {
                if(err) console.log(err);

                var b = bicicleta.createInstance(2, 'roja', 'montaña', [-34.252525, -58.846589]);
                bicicleta.add(b, (err, newBicis) => {
                    if(err) console.log(err);

                    bicicleta.allBicis((err, bicis) => expect(bicis.length).toBe(2));
                    bicicleta.removeByCode(2, (err, delBici) => {
                        if(err) console.log(err);
                        bicicleta.allBicis((err, bicis) => expect(bicis.length).toBe(1));
                        done();
                    });
                    
                });
            });
        });
    });
});