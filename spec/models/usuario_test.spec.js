var mongoose = require('mongoose');
var Usuarios = require('../../models/usuarios');
var Reservas = require('../../models/reservas');
var Bicicletas = require('../../models/bicicletas');

describe('Testing Usuarios/Reservas...', () => {
    beforeEach((done) => {
        mongoose.connection.close();
        var mongoDB = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', () => console.log('We are connected to the database!'));  
        done();
    });

    afterEach((done) => {
        Reservas.deleteMany({}, (err, success) => {
            if(err) console.log(err);
            Usuarios.deleteMany({}, (err, success) => {
                if(err) console.log(err);
                Bicicletas.deleteMany({}, (err, success) => {
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando se realiza un reserva', () => {
        it('Debe existir la reserva', (done) =>{
            const usuario = new Usuarios({ usuario: "Jonathan Fox"});
            usuario.save();

            const bicicleta = new Bicicletas({ "code": "10", "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58});
            bicicleta.save();

            var dateStart = new Date();
            var dateEnd = new Date();
            dateEnd.setDate(dateStart.getDate() + 1);

            usuario.reservar(bicicleta.id, dateStart, dateEnd, (err, reserva) =>{
                Reservas.find({}).populate('bicicleta').populate('usuario').exec((err, reservas)=>{
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(10);
                    expect(reservas[0].usuario.usuario).toBe(usuario.usuario);
                    done();
                });
            });

        });
    });
});