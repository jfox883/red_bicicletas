var mongoose = require('mongoose');
var bicicleta = require('../../models/bicicletas');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Bicicletas...', () => {
    beforeEach((done) => {
        mongoose.connection.close();
        var mongoDB_Test = 'mongodb://localhost/testDB';
        mongoose.connect(mongoDB_Test, { useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', () => console.log('We are connected to the database!'));  
        done();
    });

    afterEach((done) => {
        bicicleta.deleteMany({}, (err, success) => {
            if(err) console.log(err);
            done();
        });
    });

    describe('GET Bicicletas', () => {
        it('Status 200', (done) => {
            bicicleta.allBicis((err, bicis) => expect(bicis.length).toBe(0));
            request.get('http://localhost:3000/api/bicicletas', (error, responnse, body) => {
                expect(responnse.statusCode).toBe(200);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Agregar una bici', (done) => {
            var header = {'content-type': 'application/json'};
            var a = '{ "code": "10", "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58}';
            request.post({
                headers: header,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: a
            }, (error,response,body) => {
                expect(response.statusCode).toBe(200);
                bicicleta.findByCode(10, (err, bici) => {
                    expect(bici.color).toBe('negro');
                });
                done();
            });
        });
    });

});